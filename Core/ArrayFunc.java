/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibjava.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

package cylibjava.Core;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by me on 2017/05/24.
 */
final public class ArrayFunc<E> {
    /// <summary>
    /// 配列を連結する
    /// </summary>
    public static <E>  List<E> Concat(E[] a, E[] b) {
        List<E> result = new ArrayList<E>(a.length + b.length);

        for(int i = 0; i < a.length; i++) result.add(a[i]);
        for(int i = 0; i < b.length; i++) result.add(b[i]);

        return result;
    }

}
