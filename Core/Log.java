/******************************************************************************************
 *  Copyright (c) 2017 Chiaki Yamada https://yamada153@bitbucket.org/yamada153/cylibjava.git
 *  Released under the MIT license
 *  http://opensource.org/licenses/mit-license.php
 ******************************************************************************************/

package cylibjava.Core;

/**
 * Created by me on 2017/05/22.
 */

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.*;
import java.text.*;
import java.util.*;
import java.io.*;
import javafx.scene.control.*;

final public class Log
{
    static  Boolean hasError;
    static  Boolean debugMode;
    static  String  fileName;
    static FileWriter fileWriter;

    static private String Now(String format)
    {
        SimpleDateFormat sd = new SimpleDateFormat(format);
        return sd.format(new Date());
    }

    /**
     * Logファイルの保存先を設定する
     */
    static public void Init(String savedir, Boolean debug) throws IOException{
        Path path = java.nio.file.Paths.get(savedir + "\\Logs");

        try {
            Files.createDirectories(path);
        }
        catch (IOException e)
        {
            throw new IOException("" + e);
        }

        fileName = path + "\\" + Now("yyyyMMddHHmmss") + ".log";

        File file = new File(fileName);
        try
        {
            fileWriter = new FileWriter(file);
        }
        catch (IOException e)
        {
            throw new IOException("" + e);
        }

        hasError = false;
        debugMode = debug;
    }

    /**
     * デバッグ用メッセージを出力する
     */
    static public void DebugLog(String msg)
    {
        try
        {
            if (debugMode)
                fileWriter.write(Now("yyyyMMddHHmmss") + "\t【デバッグ】\t" + msg);
        }
        catch (IOException e) {
            System.out.print("IOExceptionが失敗しました。デバッグログの書き込みに失敗しました。");
        }
    }

    /**
     * 警告メッセージを出力する
     */
    static public void WarningLog(String msg)
    {
        try {
            fileWriter.write(Now("yyyyMMddHHmmss") + "\t【警告】\t" + msg);
        }
        catch (IOException e) {
            System.out.print("IOExceptionが失敗しました。警告ログの書き込みに失敗しました。");
        }
    }

    /**
     * エラーメッセージを出力する。この関数が一回でも呼ばれると、アプリケーション終了時にログが表示される
     */
    static public void ErrorLog(String msg)
    {
        hasError = true;

        try {
            fileWriter.write(Now("yyyyMMddHHmmss") + "\t【エラー】\t" + msg);
        }
        catch (IOException e) {
            System.out.print("IOExceptionが失敗しました。エラーログの書き込みに失敗しました。");
        }
    }

    /**
     * Logを終了する
     */
    static public void Close(){
        try{
            fileWriter.close();
            if (hasError)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR , "" , ButtonType.OK);
                alert.getDialogPane().setContentText( "ログがあります。" );
                alert.showAndWait();
            }

            File file = new File(fileName);
            if(file.length() == 0) file.delete();
        }
        catch (IOException e){
            //最低の作法だと自覚しております。
        }
    }
}
