package cylibjava.Test;

import cylibjava.Core.*;
import javafx.scene.control.TextArea;

/**
 * Created by me on 2017/05/25.
 */
final public class TestArrayFunc {
    static public void Distinct(TextArea memo)
    {
        String[] a = new String[]{"test1", "test2", "test3"};
        String[] b = new String[]{"test4", "test5", "test6"};
        String[] c = ArrayFunc.Concat(a, b).toArray(new String[0]);

        for(String s: c)
            memo.appendText(s + "\n");
    }
}
