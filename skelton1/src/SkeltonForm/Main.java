package SkeltonForm;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.event.ActionEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.application.Platform;

import java.io.File;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("SkeltonForm.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 580, 250));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @FXML
    private void Close(ActionEvent event) {
        Platform.exit();
    }
    @FXML
    private void SelectLoadFile(ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.setTitle("読込みファイル選択");
        File importFile = fc.showOpenDialog(null);
        System.out.print(importFile);
    }
    @FXML
    private void SelectSaveFile(ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.setTitle("保存ファイル選択");
        File importFile = fc.showSaveDialog(null);
        System.out.print(importFile);
    }
    @FXML
    private void SelectLoadFolder(ActionEvent event) {
        DirectoryChooser dc = new DirectoryChooser();
        dc.setTitle("読込みフォルダ選択");
        File importFile = dc.showDialog(null);
        System.out.print(importFile);
    }
    @FXML
    private void SelectSaveFolder(ActionEvent event) {
        DirectoryChooser dc = new DirectoryChooser();
        dc.setTitle("保存先フォルダ選択");
        File importFile = dc.showDialog(null);
        System.out.print(importFile);
    }
}
